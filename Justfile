default: all

all: build view

view:
  zathura cv.pdf

build:
  typst compile cv.typ

gp:
  git add . && git commit -m "." && git push
