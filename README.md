# Chase Brown's Curriculum Vitae
<picture>
  <source media="(prefers-color-scheme: dark)" srcset="https://chasealanbrown.github.io/images/logo_dark.svg">
  <img alt="Signature of Chase Brown." src="https://chasealanbrown.github.io/images/logo.svg">
</picture>

<!--[logo](https://chasealanbrown.github.io/images/logo.svg)-->

Written in Typst.

1. [Installation & Usage](#computer-usage)
2. [License](#scales-license)

## :floppy_disk: Installation & Usage

Create the CV using `just`, which runs `typst compile`.

```bash
just
```

## :scales: License

`LICENSE` file applies to the Typst template & scripts.