#import "./template.typ": base_layout, put_keyword_dump_in_there, default_style, lenk, skilltag

// Perhaps load different styles here
#let cab_style = default_style

// Page layout
#let cv_project(
  style: cab_style, cvdata,
  //self,
  lcol, rcol,
) = {
  set page(
    style.page.paper, margin: style.page.margin, fill: style.colors.page_background, background: none,
  )
  set text(
    font: style.font.body, weight: style.font.weight, size: style.size.base, fill: style.colors.text, hyphenate: style.font.hyphenate,
  )
  show heading: it => {
    set text(fill: style.colors.heading)
    it
  }
  show heading.where(level: 1): it => {
    set text(size: style.size.h1)
    it
  }
  show heading.where(level: 2): it => {
    set text(size: style.size.h2)
    upper(it)
    v(-4mm)
    line(length: 100%, stroke: style.line_height + style.colors.dark)
  }
  show heading.where(level: 3): it => {
    set text(size: style.size.h3)
    smallcaps(it)
    //v(-4mm)
    //sym.dash.em
  }
  show heading.where(level: 4): it => {
    set text(size: style.size.h4, weight: "bold")
    it
  }
  show heading.where(level: 5): it => {
    v(-3mm)
    set text(size: style.size.h5, weight: "thin", fill: style.colors.text_lighter)
    emph(it)
  }

  put_keyword_dump_in_there()

  let header = {
    grid(
      columns: (1fr, auto), [
        = #cvdata.cv.name
        #v(-2mm)
        #text(fill: style.colors.base)[*#cvdata.cv.label*]
      ], [
        #set block(spacing: 5pt)
        #table(
          columns: 1, stroke: none, inset: 0pt, align: (right), row-gutter: 3pt, column-gutter: 4pt, [
            #block[
              #text(
                fill: style.colors.text_lighter, weight: "semibold",
              )[*#cvdata.cv.email*]
            ]
            #block[
              #text(
                fill: style.colors.text_lighter, weight: "semibold",
              )[*#cvdata.cv.phone*]
              // Probably don't need address if willing to move
              //#text(fill: style.colors.text_lighter)[*#cvdata.cv.location*]
            ]
            #for sn in cvdata.cv.social_networks {
              let git = ""
              let url = ""
              let username = sn.username
              if (lower(sn.network) == "github") {
                url = "https://github.com/" + sn.username
                git = "hub"
              } else if (lower(sn.network) == "gitlab") {
                url = "https://gitlab.com/" + sn.username
                git = "lab"
              } else if (lower(sn.network) == "linkedin") {
                url = "https://linkedin.com/in/" + sn.username
                git = "linkedin"//linebreak()
                username = "chase-brown"
              } else {
                url = "https://" + sn.url
              }
              block()[
                #h(1em) * #lenk(url, username, icon: git, font: style.font.heading) *
              ]
            }
          ],
        )
      ],
    )
    set block(spacing: 5pt)
    align(
      center,
    )[
      #box(
        inset: 4pt, fill: style.colors.base, baseline: 2pt, radius: 4pt, width: 100%,
      )[
        #set text(fill: white)
        #grid(
          columns: (1fr, auto),
          // [#lenk("https://chasebrown.io", cvdata.cv.website, fill:white)],
          //[#lenk("https://chasebrown.io", [chasebrown.io], fill:white),
          [#lenk("https://cablab.cx", [cablab.cx], fill: white)], lenk(
            "https://gitlab.com/chase_brown/cv_typst", [CV repo], icon: "gitfork", size: 8pt, fill: white,
          ),
        )
      ]
    ]
  }
  base_layout(style: style, header, lcol, rcol)
}
