// Refined / edited from:
//   Source: https://git.xenua.me/xenua/cv-typst/src/branch/main/cv.typ

#import "@preview/fontawesome:0.1.0": *

#let langicons = (
  Python: "./asset/python.png",
  Bash: "./asset/bash.png",
  Julia: "./asset/julia.png",
  SQL: "./asset/sql.jpg",
  R: "./asset/R.jpg",
  Typst: "asset/typst.png",
  Go: "asset/go.png",
  LaTeX: "asset/tex.png",
  "Matlab/Octave": "asset/matlab.png",
  Rust: "asset/rust.png",
)

#let TeX = style(styles => {
  set text(font: "New Computer Modern")
  let e = measure("E", styles)
  let T = "T"
  let E = text(1em, baseline: e.height * 0.31, "E")
  let X = "X"
  box(T + h(-0.15em) + E + h(-0.125em) + X)
})

#let LaTeX = style(styles => {
  set text(font: "New Computer Modern")
  let a-size = 0.66em
  let l = measure("L", styles)
  let a = measure(text(a-size, "A"), styles)
  let L = "L"
  let A = box(scale(x: 105%, text(a-size, baseline: a.height - l.height, "A")))
  box(L + h(-a.width * 0.67) + A + h(-a.width * 0.25) + TeX)
})

#let process_colors(colors: (:)) = {
  if colors.at("base", default: none) == none {
    colors.insert("base", rgb("#951010"))
  }
  if colors.at("light", default: none) == none {
    colors.insert("light", colors.base.lighten(35%))
  }
  if colors.at("lighter", default: none) == none {
    colors.insert("lighter", colors.base.lighten(70%))
  }
  if colors.at("dark", default: none) == none {
    colors.insert("dark", colors.base.darken(35%))
  }
  if colors.at("darker", default: none) == none {
    colors.insert("darker", colors.base.darken(70%))
  }
  return colors
}

#let _default_colors() = {
  let c = process_colors()
  let textc = rgb("#382224")
  c.insert("text", textc)
  c.insert("heading", textc.darken(90%))
  c.insert("text_lighter", textc.lighten(25%))
  c.insert("page_background", white)
  return c
}

#let default_style = (
  left_col_size: 70%,
  col_gap: 8mm,
  line_height: 0.3mm,
  page: (
    paper: "a4",
    margin: 1cm,
  ),
  font: (
    heading: "DM Serif", //Tinos  //"Inter"//"Rosarivo"
    body: "DM Serif",  //"New Computer Modern"  //"Cormorant Garamond" //"Liberation Sans"  //"Tinos"  //"DM sans 9pt"//"Rosarivo"
    weight: "regular", //"thin", "extralight", "light", "regular", "medium", "se mibold", "bold", "extrabold", or "black"
    w2: "semibold",
    hyphenate: false,
  ),
  size: (
    // font sizes
    h1: 36pt,
    h2: 14pt,
    h3: 12pt,
    h4: 11pt,
    h5: 10pt,
    base: 10pt,
    small: 9pt,
  ),
  par: (
    leading: 6pt,
    justify: true,
  ),
  list: (
    spacing: 3pt,
  ),
  exp_bar: (
    height: 3mm,
    width: 32mm,
    corner_radius: 1mm,
    bar_offset: -0.5mm,
    subtext_offset: -2mm,
  ),
  colors: _default_colors(),
)


#let tag(body, fill: none, tag: "") = {
  box(inset: 3.5pt,
      fill: fill,
      baseline: 2pt,
      radius: 4pt,
      stroke: 1pt + default_style.colors.base.lighten(20%),
      )[
    #text(size: default_style.size.small - 2pt,
          fill: white, 
          strong(body)) //#smallcaps(text(white, strong(upper(tag))))
  ]
}
#let skilltag = tag.with(fill: default_style.colors.base.lighten(30%),
                         tag: "")

#let lenk(url, txt, icon: "", font: "Inter", fill:
  default_style.colors.text_lighter, weight: 400, size: default_style.size.base) = {
    
    txt = text(font: font,
               fill: fill,
               weight: weight,
               size: size)[#txt]
    if icon == "lab" {
        link(url)[#box(image("../asset/gitlab.png", height:
    default_style.size.base),
        baseline: 1pt) #txt #fa-link()]
    }
    if icon == "hub" {
        link(url)[#box(image("../asset/github.png", height:
    default_style.size.base),
        baseline: 1pt) #txt #fa-link()]
    }
    if icon == "linkedin" {
        link(url)[#box(image("../asset/linkedin.jpg", height:
    default_style.size.base),
        baseline: 1pt) #txt #fa-link()]
    }
    if icon == "gitfork" {
        link(url)[#box(image("../asset/gitfork.png", height:
    default_style.size.base),
        baseline: 1pt) #txt #fa-link()]
    }
    if icon == "" {
        link(url)[#fa-link() #txt]
    }
}

#let base_layout(
  style: default_style,
  header,
  left,
  right,
) = {
  [#header]
  grid(
    columns: (style.left_col_size, 1fr),
    gutter: style.col_gap,
    [#left], [#right]
  )
}

#let education_item(edu) = {
  let start = edu.start_date//utils.strpdate(edu.start_date)
  let end = edu.end_date// utils.strpdate(edu.end_date)
  let edu-items = ""
  if edu.highlights != none {
    for hi in edu.highlights {
        edu-items = edu-items + "- " + hi + "\n"
    }
  }
  if edu.honors != none {edu-items = edu-items + "- *Honors*: " + edu.honors.join(", ") + "\n"}
  // create a block layout for each education entry
  block(width: 100%, breakable: true)[
  // Degree in Field
  #text(weight: "extrabold", size: default_style.size.base+2pt)[#edu.study_type #sym.dash.em #text(style:
  "italic")[#edu.area]] #h(1fr) \
  // Institution (Location)
  #text(size: default_style.size.base - 1pt)[*#edu.institution* #text(style:
  "italic", weight: "extralight")[(*#edu.location*)]] \
  // Start - End Dates
  #start #sym.dash.en #end \
  // Focus
  #if edu.focus != none {
      text(weight: "bold", default_style.size.base + 0.5pt)[Focus: #h(0.3em) #edu.focus]
  }
  // Highlights
  #eval(edu-items, mode: "markup")
  ]
}

#let work_item(w) = {
  block(width: 100%, breakable: false)[
    #text(weight: "bold", size: default_style.size.h2)[#eval(w.organization, mode: "markup")] \
    //=== *#eval(w.organization, mode: "markup")* //#h(1fr) *#w.location* \
    *#w.location* \
  ]
  // create a block layout for each work entry
  let index = 0
  for p in w.positions {
    if index != 0 {v(0.6em)}
    block(width: 100%, breakable: true, above: 0.6em)[
      // parse ISO date strings into datetime objects
      #let start = p.start_date // utils.strpdate(p.startDate)
      #let end = p.end_date//utils.strpdate(p.endDate)
      // line 2: position and date range
      #text(style: "italic")[#p.position] #h(1fr)
      #start #sym.dash.en #end \
      // highlights or description
      #for hi in p.highlights [
        //#block(width: 100%, breakable: true)[
        - #eval(hi, mode: "markup")
        //]
      ]
    ]
    index = index + 1
  }
}


#let pub_item(pub) = {
  let date = pub.releaseDate
  let jrl = ""
  if "rxiv" in lower(pub.publisher) [
    #block(width: 100%, breakable: false)[
       - #eval(pub.authors, mode: "markup") "#emph(pub.name)" #text(style: "italic")[Preprint on #pub.publisher]  #h(1fr) #date
    ]
  ] else [
    #block(width: 100%, breakable: false)[
       - #eval(pub.authors, mode: "markup") "#emph(pub.name)" #underline(pub.publisher)  #h(1fr) #date
    ]
  ]
}

#let mentor_item(mi) = {
  block(width: 100%, breakable: true)[
    ==== #mi.institute  \
    #for p in mi.persons {
       [- *#p.name* (#p.role) \ #p.project]
    }
  ]
}

#let teach_item(c) = {
  block(width: 100%, breakable: true)[
    ==== #c.role:  #emph(c.name)  
    ===== #c.institution #h(1fr) #c.date \
    #c.content
  ]
}

#let skill_item(cat) = {
  show par: set par(leading: 2.0pt)
  [=== #cat.category ]
  if cat.skills == none { }
  else {
    for skill in cat.skills {
      [ *#skilltag(skill)*]
    }
  }

  if cat.subcategories == none { }
  else {
    for subcat in cat.subcategories {
      block(breakable: true)[_ #subcat.subcategory: _ ]
      v(-0.8em)
      if subcat.skills == none { }
      else {
        for item in subcat.skills {
          [ #skilltag(item)]
        }
        v(-0.8em)
      }
    }
  // WARN: This is incredibly hacky and I hate it
  if cat.category == "Engineering" {
    v(1em)
    colbreak()
    [== Skills ]
  }
  }
}

#let extracurricular_item(p) = {
  let r = ""
  let h = ""
  let d = ""
  if p.role != none {
     r = text()[(#p.role)]
  } 
  if p.date != none {
     d = text()[\[#p.date\]]
  }
  block(width: 100%, breakable: true)[
    - *#p.organization* #r #d
  ]
  //if p.highlights != none {
  //  v(-0.5em)
  //  block(width: 100%, breakable: true)[
  //  #for hi in p.highlights { [ - #hi ] } ] } }
}

#let course_item(c) = {
  block(width: 100%, breakable: true)[
    === *#c.institute* \
    #for a in c.areas {
      [ - * #a.field * ]
      list(body-indent: 2em, marker: none)[ 
        #a.courses.map(((k))=>k.name).join(" | ") 
      ]
    }
  ]
}

#let present_item(pub) = {
  let date = pub.releaseDate
  let jrl = ""
  if "poster" in lower(pub.role) [
    #block(width: 100%, breakable: false)[
       - "#emph(pub.name)" #eval(pub.authors, mode: "markup")//  #emph([(Poster)])
       #h(1fr) #underline(pub.avenue) #date
    ]
  ]
  else [
    #block(width: 100%, breakable: false)[
      - * #pub.role * "#emph(pub.name)" 
      #h(1fr) #underline(pub.avenue) #date
    ]
  ]
}

#let award_item(aw) = {
  let date = aw.date
  block(width: 100%, breakable: false)[
    #if aw.url != none [
      - * #link(aw.url)[#aw.title] * \
    ] else [
      - *#aw.title* \
    ]
    #text(style: "italic")[#aw.issuer]  #h(1fr) #date \
    #if aw.location != none {
      text(style: "italic")[#aw.location]
    }
    #if aw.highlights != none {
      for hi in aw.highlights [
        - #eval(hi, mode: "markup")
      ]
    }
  ]
}

#let exp_bar(
  style: default_style,
  filled,
  body
) = {
  box(
    grid(
      columns: (1fr, auto),
      [#body],
      {
        v(style.exp_bar.bar_offset)
        box(
          width: style.exp_bar.width,
          height: style.exp_bar.height,
          fill: style.colors.lighter,
          radius: style.exp_bar.corner_radius,
          box(
            width: filled,
            height: style.exp_bar.height,
            fill: style.colors.base,
            radius: style.exp_bar.corner_radius,
          )
        )
      }
    )
  )
}

#let machine_reading_keyword_dump = [ // this actually does help and i hate that
    // based in part on the stackoverflow dev survey
    Unix, GNU, Linux, Archlinux, Alpinelinux, Debian, FreeBSD, OpenBSD, BSD, TrueNAS OPNSense,
    Administration, Scalable, Server, SAAS, Network, Backend, High performance Async, Serverless,
    Database, SQLite, DuckDB, SQL, NoSQL, MongoDB, KuzuDB, LanceDB,
    Qdrant, Faiss, MySQL, Elasticsearch, Neo4j, Iroh,
    Programming, Python, Julia, R, Lua, Bash, Typst, LaTeX, Matlab, Octave,
    Go, Rust, Haskell, Nim, C++, OriginC, Fortran,
    Development, Jupyter, Pandas, Polars, Spark, Numpy, Scipy, Matplotlib,
    Seaborn, Plotly, Bokeh, Flask, Keras, Tensorflow, PyTorch,
    Qemu, Flatpak, Docker, Podman, Container,
    Git, CI/CD, DevOps, Observability,
    Tmux, tmux, Vim, vim, Neovim, Astronvim, AwesomeWM, i3, Sway, XMonad, Qtile, DWM, Pinnacle, SwayWM,
    manim, quarto,
    Zsh, Bash, POSIX, Shell,
    tty, Alacritty, grep, ripgrep, lazygit, git, bat, sed, awk, jq, fzf, ripgrep, wireguard, cryptography, security, privacy Tailscale, IPFS,
    Syncthing, Samba, NFS, SSH, SCP, rsync, Rclone, Rsync
    Transmission Electron Microscopy, TEM, Scanning Electron Microscopy, SEM, Atomic Force Microscopy, AFM,
    Optical Absorption Spectroscopy, Fluorescence Spectroscopy,
    Photoluminescence Spectroscopy, Raman Spectroscopy, Multi-photon
    Fluorescence Microscopy, Confocal Microscopy, Tunable Laser, Fourier Transform Infrared Spectroscopy, FTIR,
    Temperature programmed reduction, TPR, Temperature programmed desorption, TPD, Thermal Gravimetric Analysis, TGA, Differential Scanning Calorimetry, DSC,
    Liquid Chromatography, LC, Gas Chromatography, GC, Mass Spectrometry, MS,
    Dynamic Light Scattering, DLS, Static Light Scattering, SLS,
    Chemical Vapor Deposition, CVD,
    Excitonic Physics, Quantum Dots, Quantum Wires, Luttinger Liquids,
    Excitons, Photonics, Nanophotonics, 
    Hetereogeneous Catalysis, Homogeneous Catalysis, Photoelectrochemistry,
    X-ray Diffraction, XRD, X-ray Photoelectron Spectroscopy, XPS, 
    Characterization, Materials Science, Solid State Physics, Physical Chemistry, Inorganic Chemistry, Organic Chemistry,
    Quantum Mechanics, Quantum Chemistry, Quantum Electrodynamics,
    Kinetics, Thermodynamics, Statistical Mechanics, Condensed Matter Physics,
    Group Theory, Persistent Homology, Algebraic Topology, Topological Data Analysis, TDA,
    Drug Discovery, Bioinformatics, Computational Biology, Structural Biology, Molecular Biology,
    Data Science, Machine Learning, Artificial Intelligence, Deep Learning, Reinforcement Learning,
    Temporal Difference Learning, Q-learning, Actor-Critic, A3C, GAN, VAE, RNN, LSTM, GRU, Transformer, BERT, GPT, T5, U-Net, ResNet,
    Neural Networks, Convolutional Neural Networks, CNN, Recurrent Neural Networks, RNN, Long Short Term Memory, LSTM, Gated Recurrent Unit, GRU,
    Neural Ordinary Differential Equations, Neural Tangents, Neural Architecture Search, NAS, Hyperparameter Optimization, Bayesian Optimization,
    Neural Homology Theory, Sheaf theory,
    Natural Language Processing, NLP, Language Modeling
    Retrieval Augmented Generation, RAG, Mamba,
    Glioblastoma, Cancer, Oncology, Immunotherapy, MGMT, IDH, EGFR, PD-L1, Tumor Microenvironment, TME, Tumor Infiltrating Lymphocytes, TILs,
    Tumor Associated Macrophages, TAMs,
    Temozolomide, let-7, micro-RNA, miRNA, DNA methylation, Drug
    Synergy,
]
#let put_keyword_dump_in_there() = {
  place(top + left, hide([#machine_reading_keyword_dump]))
}
