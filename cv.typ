#import "@preview/fontawesome:0.1.0": *
#import "misc/template.typ": *
#import "misc/layout.typ": cv_project, cab_style

// Load the content from yaml
#let cvdata = yaml("cv.yml")
#let style = cab_style


#let part = (
  summary: [
    #show par: set block(spacing: 10pt)
    == Summary
    #eval(cvdata.cv.summary.main, mode: "markup")
    //#par( leading: 0.6em, justify: true)[#eval(cvdata.cv.summary.goal, mode: "markup")]
    #par( leading: 0.6em, justify: true)[#eval(cvdata.cv.summary.expertise, mode: "markup")]
  ],

  education: [
    == Education
    #for edu in cvdata.cv.education {
      education_item(edu)
    }
  ],

  work: [
    == Work Experience
    #for w in cvdata.work {
       work_item(w)
    }
  ],

  research: [
    == Publications
    #for pub in cvdata.publications {
        pub_item(pub)
    }
  ],

  present: [
    == Presentations
    #for pub in cvdata.presentations {
        present_item(pub)
    }
  ],

  mentor: [
    == Mentorship
    #for m in cvdata.teaching_mentorship {
        mentor_item(m)
    }
  ],

  teaching: [
    == Teaching
    #for m in cvdata.teaching.courses {
        teach_item(m)
    }
  ],

  // extracurricular: [
  //   == Extracurricular
  //   #for ec in cvdata.extracurricular_activities {
  //       extracurricular_item(ec)
  //   }
  // ],

  courses: [
    == Relevant Coursework
    #for c in cvdata.relevant_courswork {
        course_item(c)
    }
  ],

  focus: [
    #show par: set par(leading: 2.0pt)
    == Expertise
    #for gen in cvdata.focus_areas {
      if gen.skills == none { }
      else {
        for skill in gen.skills {
          [ *#skilltag(skill, fill: default_style.colors.base)*]
        }
      }
    }
  ],

  skills: [
    == Skills
    #for cat in cvdata.skills {
      block(breakable: true, spacing: 0.5em)[
        #skill_item(cat)
      ]
    }
  ],

  lang: [
    == Languages
    #for lang in cvdata.languages {
      
      block(breakable: true, spacing: 0.5em)[
        #exp_bar(int(lang.fluency)*10%)[#lang.language]
        //#exp_bar(int(lang.fluency)*10%)[
        //#grid(columns: (1fr, 2.0em))[#lang.language  ][#image(langicons.at(lang.language), height:
        //        style.size.base)]
        //    ]
      ]
    }
  ],
  
  // awards: [
  //   == Awards
  //   #for aw in cvdata.awards {
  //     block(breakable: true, spacing: 0.5em)[
  //       #award_item(aw)
  //     ]
  //   }
  // ],
)


// left column
#let lcol = [
  #part.summary
  #part.education
  #part.work
  #part.research
  #part.present
  #part.mentor
  #part.teaching
  #part.courses
]
// right column
#let rcol = [
  #part.focus
  #part.lang
  #part.skills
  #colbreak()
  // #part.extracurricular
  // #part.awards
]

#cv_project(
  style: style, //self,
  cvdata, 
  lcol, rcol
)
